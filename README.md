build
=====

 - update export.cfg
 - run ./export.sh
 - if exports have failed, follow up why

upload
======

 - dch -i to create a new changelog entry
 - dpkg-buildpackage -S to build the source package
 - dput to upload

export.sh
=========

This is a nice wrapper around upstream's wiki2docbook tool, handling mass
export of pages, CMakeLists generation as well as basic sanity checking.

The script takes care of:

- getting the upstream tooling
- building a list of possible localized pages to export
- gets the wiki XML export 
- converts the export to docbook using wiki2docbook
- checks validity of the docbook using meinproc4
- generates CMakeLists.txt accordingly

Translations that fail the meinproc4 sanity check will be discard as to
not have useless stuff in the source package.

misc
====

Please note that the actual export data is ignored by git and must be exported
before doing an upload. This is to both ensure sanity of the git repo and
making it less likely that one uploads outdated data by accident.
